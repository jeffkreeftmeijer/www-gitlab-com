You can help to improve the [about.gitlab.com](https://about.gitlab.com/) website by sending merge requests to this repository. Thank you for your help!

By submitting code as an individual you agree to the [individual contributor license agreement](doc/legal/individual_contributor_license_agreement.md).
By submitting code as an entity you agree to the [corporate contributor license agreement](doc/legal/corporate_contributor_license_agreement.md).

---
layout: markdown_page
title: "Hiring"
---

## New hires

Steps:

1. Create issue for new hire in organizatoon with following checklist:
1. Signed PIAA in Dropbox
1. Signed contract in Dropbox
1. Scan of photo id in Dropbox
1. Put into Profiles in Lastpass
1. Create Google account, firstname@gitlab.com or initial(s)@gitlab.com
1. Add to Slack
1. [Add to Lastpass](https://lastpass.com/enterprise_create.php)
1. Add to Recurly
1. Add to GitLab Dropbox
1. Add to BV and Inc Dropbox (fianance only)
1. [Add to Mailchimp](https://us5.admin.mailchimp.com/account/users/) (if sales or finance)
1. Add to [QuickBooks users](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/handbook/hiring/index.md) (finance only)
1. Add to Comerica (finance only, viewer only)
1. Gitlab.com account invited to gitlab.com group
1. GitLab.org account created and invited to gitlab group
1. /cc in the gitlab issue in the organization
1. Invite to team meeting
1. Invite to sales meeting
1. Invite to autoconnect on Beamy
1. TODO Add more
---
layout: default
title: GitLab High Availability
---
<div id="content">
  <div class="container txt-lefty">
    <div class="row">
      <div class="col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
        <h3>What is High Availability?</h3>
        <p>
            High availability is a system design that ensures a prearranged level of operational performance throughout a specific time-period. The most common way to measure HA is through the notion of uptime, which measures how long a service is up and running.
        </p>
        <p>
            GitLab provides a service that is usually essential to most organizations: it enables people to collaborate on code in a timely fashion. Any downtime should therefore be short and planned. Luckily, GitLab provides a solid setup even on a single server without special measures. And because of the distributed nature of git developers can still commit code locally even when GitLab is not available. However, some GitLab features such as the issue tracker and Continuous Integration are not available when GitLab is down.
        </p>
        <p>
            <b>Keep in mind that all High Availability solutions come with a trade-off between cost/complexity and uptime</b>. The more uptime you want, the more complex the solution. And the more complex the solution, the more work is involved in setting up and maintaining it. High availability is not free and every HA solution should balance the costs against the benefits.
        </p>
        <h3>High Availability for you</h3>
        <p>
            The two questions that you need to ask yourself are:
            </p><ol>
            <li>What are my specific needs for code availability?</li>
            <li>What complexity level can my organization handle?</li>
            </ol>
        <p></p>
        <p>
            HA solutions range from a cheap and simple back-up to highly complex and costly master-master configurations. When it comes to HA, there is unfortunately no way to split between an increase in complexity and an increase in cost.
        </p>
        <p>
            We recommend that you thoroughly analyze the benefits of an HA solution against its costs. Most organizations select a High Availability solution for GitLab that is less complex than the one they use for their primary customer-facing infrastructure that directly generates revenue. <b>It is important that you always choose something that you have experience with and will test regularly</b>. A badly implemented HA solution causes more downtime than it solves. For example the <a href="http://www.drbd.org/users-guide/s-gfs-create-resource.html">DRBD user guide</a> states: "By configuring auto-recovery policies, you are effectively configuring automatic data-loss!". It is more cost effective to go from a simple solution to a more complex one than the other way around. Therefore, we suggest you work your way up from the simplest solution and see what works best for you.
        </p>
        <h3>Maturity levels</h3>
        <h4>1. Backup of files and Database</h4>
        <p>
            It is possible to automatically backup the GitLab repositories, configuration and the database. You can manage backups with the <a href="https://github.com/gitlabhq/gitlabhq/blob/master/lib/tasks/gitlab/backup.rake">GitLab backup tasks</a>: create a backup with `rake gitlab:backup:create` and restore it with `rake gitlab:backup:restore`. In case the server becomes unavailable, then the backup can be used to restore the GitLab installation. This solution is appropriate for teams that have a single metal server at their disposal. <b>We recommend you store the backup at an external location. Also we recommend automating the backup task and monitoring successful completion.</b>
        </p>
        <h4>2. Recovery from snapshot</h4>
        <p>
            It is possible to create a snapshot of your working server. The snapshot is a copy of all the files on the server, and is taken periodically. In case of failure, the server is manually reverted to the latest available snapshot. If a secondary location is available, then it is possible to automatically revert to the snapshot, without the need for human intervention.
        </p>
        <p>
            It is common to do this with virtual machines; most providers have functionality for taking snapshots. However it is also possible to use the XFS filesystem to take snapshots.
        </p>
        <p>
            Recovering from a snapshot is faster than recovering from a backup as described above. In the latter case, you’d commonly need to re-install GitLab, manually restore any customization, and only then then run `rake gitlab:backup:restore`. Restoring from a snapshot skips these extra manual steps.
        </p>
        <p>
            If you use the snapshot to restore the GitLab server to another machine one should consider the IP address implications. To make sure everyone can quickly use the new server it is common to update the DNS entry. In this case either the new machine has to use a new IP address, or the snapshot should use the same OpenSSH server certificate as the server it is replacing. Otherwise users accessing GitLab using the Git client over SSH will see “WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED”, because the server certificate and the IP address of the GitLab server do not match.
        </p>
        <p>
            The IP address mismatch issue can be resolved by using a floating IP, which is IP failover in a router, but has to be supported both by the router device and the network. If the new address is in a secondary geographical location, both locations need to be on the same network and have the same core router.
        </p>
        <p>
            <b>Using recovery from snapshots is best suited for single virtual servers.</b>
        </p>
        <h4>3. Multiple application servers</h4>
        <p>
            It is possible to create a split between the application server, the file system and the DBMS. If resources are available, you can use a load balancer to assign tasks to multiple application servers. This way, if one of the application servers fails, the workflow is not interrupted, and at most the workflow gets slower as the load is shared between less machines.
        </p>
        <p>
            The setup is described in the below picture.
            <img src="../images/Config_LB_appservers.png" alt="gitlab.com">
        </p>
        <p>
            This solution also requires a shared file system (NFS), so it is only available for GitLab versions 6.0 and up, where satellites are in the shared disk. At this point, you should consider putting the filestore and DBMS on separate machines as well. Moreover, the filestore and the DBMS will need recovery protocols on their own (through snapshots or backups)
        </p>
        <h4>4. Single slave server</h4>
        <p>
            <b>If you only want to use two machines for your HA solution, we recommend keeping one as a slave of the other.</b> In this situation, each machine has the full GitLab repositories, configuration and the database. The slave server is a copy of the master. You can use Distributed Replicated Block Device (DRBD) to ensure that all data from the master is replicated to the slave in a timely manner.
        </p>
        <p>
            Manual failover is done by mounting the file storage on the slave, which is turned into a master. At this point, the unavailable server should be turned off or taken off the network, to avoid it coming online with master status.
        </p>
        <p>
            This option is feasible both for users who want to keep their slave server in the same data center, or if they want it in a different geographical location.
        </p>
        <h4>5. Filestore and DBMS slave servers</h4>
        <p>
            <b>It is not possible to combine the multiple application servers solution with a single slave server.</b> In this situation you need to have a second site with multiple stateless application servers and a filestore and DBMS slave server.
        </p>
        <p>
            In this situation, users still connect via the load balanced application servers, but the filestore and the DBMS are copied to a slave server(s) in the same datacenter or in another location. Distributed Replicated Block Device (DRBD) can be used to pass data from the master filestore and DBMS servers to the slaves.
        </p>
        <p>
            There is very little state on the application server in GitLab, so we don’t recommend having a slave application server alongside the filestore and the DBMS. Using a load balancer results in the same workflow impact, but in a more simple manner.
        </p>
        <p>
            Automated failover can be achieved with pacemaker alongside STONITH network management. Keep in mind that application servers need to be prepared for transitioning to the new network addresses.
        </p>
        <p>
            In this situation you can also opt to synchronize the database via a database specific protocol instead of DRBD, in the documentation for each database you can find out more about the <a href="http://dev.mysql.com/doc/mysql-ha-scalability/en/ha-overview.html">options for MySQL</a> and the <a href="http://www.postgresql.org/docs/9.2/static/high-availability.html">options PostgreSQL</a>.
        </p>
        <h4>6. Clustered/master-master filestore and DBMS</h4>
        <p>
            Clustered solutions are also known as distributed solutions. For the purpose of GitLab a master/master solution falls in the same category. Clustered solutions are harder to setup and maintain than master-slave solutions. But they can offer more resilience to failures and allow you to grow beyond the capacity of a single filestore or dbms server.
        </p>
        <p>
            Clustered solutions are available in two modes, synchronous and asynchronous. A synchronous clustered configuration needs to propagate each change to every server, a time-consuming process. This is a fundamental drawback of synchronous solutions, it is only practical when the servers are geographically close. Otherwise the system becomes very slow to the end user.
        </p>
        <p>
            The speed of the process can be improved on condition that communication happens asynchronously. But in this case there can be incompatible changes on both servers, for example two accounts with the same username can be created. GitLab is not designed to resolve these situations and asynchronous communication is unfortunately not an option for GitLab.
        </p>
        <p>
            Filestore HA can be achieved either through multiple <a href="http://en.wikipedia.org/wiki/Clustered_file_system">clustered file systems</a>, such as    <a href="http://en.wikipedia.org/wiki/GFS2">GFS2</a>, <a href="http://en.wikipedia.org/wiki/VMware_VMFS">VMFS</a> and <a href="http://ceph.com/">Ceph</a>. Alternatively you can use a horizontally scaling appliance, for example a scale-out NAS from    <a href="http://www-03.ibm.com/systems/storage/network/sonas/">IBM</a> or <a href="http://www.emc.com/storage/isilon/isilon.htm">EMC Isilon</a>. For the DBMS there are also multiple options: Oracle’s <a href="http://www.mysql.com/products/cluster/">MySQL Cluster</a> and    <a href="http://wiki.postgresql.org/wiki/Replication,_Clustering,_and_Connection_Pooling">PostgreSQL's clustering solutions</a>.
        </p>
        <p>
            In the future GitLab might start using <a href="https://github.com/libgit2/libgit2-backends">libgit2-backends</a> to store the Git repositories in the database. In this case one clustered database can store all the state of the GitLab application. This would make a clustered system easier to set up. Unfortunately libgit2 and its ruby bindings <a href="https://github.com/libgit2/rugged">Rugged</a> do not contain all the functionality GitLab needs yet.
        </p>
        <p>
            While clustered or master/master configuration support is not part of the GitLab roadmap yet, there is good news. <b>Different company HA needs can lead to alternative solutions that are simpler and cheaper.</b> For example:
            </p><ul>
            <li>If you need write HA and fast recovery, any of the previous HA setups can be used.</li>
            <li>If you need to locally clone large repo’s, then there is a third-party mirroring solution available for GitLab,            <a href="https://github.com/sag47/gitlab-mirrors">gitlab-mirrors</a></li>
            <li>If you just need to be able to push to multiple GitLab servers in different locations, you should consider using git remotes.</li>
            <li>If what you need is a fast user experience for people working from different continents, using local git clients (command line, Tower, etc) can help.</li>
            </ul>
        <p></p>
        <h4>Please get in touch</h4>
        <p>
            Supporting High Availabily setups is something we take very serious at GitLab.com. The information we provide here is just a rough guide designed to help you get a feel of the hardware, software and process implications of different HA setups. When it comes to your specific situation, we prefer to assess it in its context. We strongly suggest you get in touch with us before setting it up. Support for HA is included with <a href="https://www.gitlab.com/subscription/">the Standard Subscription</a>. If you have any questions please <a href="mailto:sales@gitlab.com">email us</a>.
        </p>
      </div>
    </div>
  </div>
</div>
